package com.example.firsthomework

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class ListActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		Log.d("test", "ListActivity -> onCreate" )
		setContentView(R.layout.activity_list)

	}

	override fun onStart() {
		super.onStart()
		Log.d("test", "ListActivity -> onStart" )
	}

	override fun onResume() {
		super.onResume()
		Log.d("test", "ListActivity -> onResume" )
	}

	override fun onRestart() {
		super.onRestart()
		Log.d("test", "ListActivity -> onRestart" )
	}

	override fun onPause() {
		super.onPause()
		Log.d("test", "ListActivity -> onPause" )
	}

	override fun onStop() {
		super.onStop()
		Log.d("test", "ListActivity -> onStop" )
	}

	override fun onDestroy() {
		super.onDestroy()
		Log.d("test", "ListActivity -> onDestroy" )
	}
}