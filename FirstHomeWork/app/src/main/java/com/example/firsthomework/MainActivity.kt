package com.example.firsthomework

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		Log.d("test","ActivityMain -> onCreate" )
		setContentView(R.layout.activity_main)

		val btn : Button = findViewById(R.id.main_activity_btn)
		btn.setOnClickListener {
			val intent = Intent(this, ListActivity::class.java)
			startActivity(intent)
		}
	}

	override fun onStart() {
		super.onStart()
		Log.d("test","ActivityMain -> onStart" )
	}

	override fun onResume() {
		super.onResume()
		Log.d("test","ActivityMain -> onResume" )
	}

	override fun onRestart() {
		super.onRestart()
		Log.d("test","ActivityMain -> onRestart" )
	}

	override fun onPause() {
		super.onPause()
		Log.d("test","ActivityMain -> onPause" )
	}

	override fun onStop() {
		super.onStop()
		Log.d("test","ActivityMain -> onStop" )
	}

	override fun onDestroy() {
		super.onDestroy()
		Log.d("test","ActivityMain -> onDestroy" )
	}
}